class Post < ActiveRecord::Base       
  has_many :comments, dependent: :destroy #Hace dependiente de los posts, si se borra uno se van todos los comments.
   validates_presence_of :title
   validates_presence_of :body
end
